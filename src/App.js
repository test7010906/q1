import Sidebar from './components/Sidebar/Sidebar';
import Content from './components/Contnent/Content'
import Footer from './components/Footer/Footer';
import './App.css';

function App() {
  return (
    <div className="App">
      <Sidebar />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
