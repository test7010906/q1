

export default function Sidebar() {
    return (
        <nav>
            <div className="menu">
                <ul>
                    <li>
                        <a href="/">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="/">
                            Service
                        </a>
                    </li>
                    <li>
                        <a href="/">
                            News
                        </a>
                    </li>
                    <li>
                        <a href="/">
                            Blog
                        </a>
                    </li>
                    <li>
                        <a href="/">
                            Contract
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

    )
}