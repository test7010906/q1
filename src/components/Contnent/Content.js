

export default function Content() {
    return (
        <div className="content">
            <img style={{ marginTop: '20px',padding:'0px 5%' }} src={require('../../assets/logoNCC 1.png')} alt='logo' />
            <div className='group2'
                style={{ backgroundColor: '#EFEFEF', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                <h3>Lorem ipsum dolor sit asmet?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique consequat placerat. Vestibulum auctor pellentesque sem, eu posuere erat hendrerit quis. Maecenas vel consequat turpis. Nam facilisis, ligula in mattis sodales, augue justo tristique nulla, sed lacinia ante eros ut mi. Morbi vitae diam augue. Aliquam vel mauris a nibh auctor commodo. Praesent et nisi eu justo eleifend convallis. Quisque suscipit maximus vestibulum. Phasellus congue mollis orci, sit amet luctus augue tristique eu. Donec vulputate odio neque, sed semper turpis pellentesque a.</p>
            </div>
            <div style={{ display: 'flex',marginTop: '20px' }} className='group3'>
                <div>
                    <h2>Lorem ipsum dolor sit asmet</h2>
                    <img align="left"  src={require('../../assets/css-icon 1.png')} alt='logo' />
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis. Donec imperdiet suscipit accumsan. Aenean consequat condimentum velit ut tempor. Nam porta massa in metus bibendum congue. Pellentesque ultrices liquam egestas nunc at
                        ullamcorper ultricies. Donec feugiat velit nulla, vel sodales est ullamcorper id. Aenean consequat condimentum velit ut tempor. Nam porta massa in metus bibendum congue. Pellentesque ultrices vestibulum mattis.
                    </p>
                </div>
                <div>
                    <h2>Lorem ipsum dolor sit asmet</h2>
                    <img align="left"  src={require('../../assets/html-icon 1.png')} alt='logo' />
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis. Donec imperdiet suscipit accumsan. Aenean consequat condimentum velit ut tempor. Nam porta massa in metus bibendum congue. Pellentesque ultrices liquam egestas nunc at
                        ullamcorper ultricies. Donec feugiat velit nulla, vel sodales est ullamcorper id. Aenean consequat condimentum velit ut tempor. Nam porta massa in metus bibendum congue. Pellentesque ultrices vestibulum mattis.
                    </p>
                </div>
                <div>
                    <h2>Lorem ipsum dolor sit asmet</h2>
                    <img align="left"  src={require('../../assets/url-icon 1.png')} alt='logo' />
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis. Donec imperdiet suscipit accumsan. Aenean consequat condimentum velit ut tempor. Nam porta massa in metus bibendum congue. Pellentesque ultrices liquam egestas nunc at
                        ullamcorper ultricies. Donec feugiat velit nulla, vel sodales est ullamcorper id. Aenean consequat condimentum velit ut tempor. Nam porta massa in metus bibendum congue. Pellentesque ultrices vestibulum mattis.
                    </p>
                </div>
            </div>
        </div>

    )
}